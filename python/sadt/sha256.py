from random import random
## SIGNAL PREPARATION
def boolrand():
    return  random() < 0.5
    
def vectorize(l):
    internal_list=[]
    for i in range(l):
        internal_list.append(boolrand())
    return internal_list
        

## 2 input NAND (base node)
def nand(i0,i1):
    return not (i0 and i1)

